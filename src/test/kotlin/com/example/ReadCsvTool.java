//package es.bcg.tfm.migraine.service;
//
//import com.opencsv.CSVReader;
//
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.FileReader;
//import java.io.IOException;
//import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.UUID;
//
//@Slf4j
//public class ReadCsvTool {
//
//  public static List<? extends Object> readCsvFile(String csvFile, Object object,
//      Field[] attributes) {
//    List<Object> result = new ArrayList<>();
//    log.debug("Attributes {}", attributes.length);
//
//    try (CSVReader reader = new CSVReader(new FileReader(csvFile))) {
//      reader.skip(1);
//      // String[] headers = reader.readNext();
//      // System.out.println("headers " + headers.length);
//      String[] line;
//      int index = 2;
//      while ((line = reader.readNext()) != null) {
//        log.debug("line {} {}", index, line.length);
//        for (int i = 0; i < attributes.length; i++) {
//          attributes[i].setAccessible(true);
//          if (line[i].equals("?")) {
//            line[i] = null;
//          }
//          if (attributes[i].getType().getSimpleName().equals("Integer")) {
//            attributes[i].set(object, Integer.valueOf(line[i]));
//          } else if (attributes[i].getType().getSimpleName().equals("Boolean")) {
//            attributes[i].set(object, Boolean.valueOf(line[i]));
//            // attributes[i].set(object,
//            // (line[i] == null ? null : (Integer.valueOf(line[i]) == 1 ? true : false)));
//          } else if (attributes[i].getType().getSimpleName().equals("Long")) {
//            attributes[i].set(object, Long.valueOf(line[i]));
//          } else if (attributes[i].getType().getSimpleName().equals("Double")) {
//            attributes[i].set(object, Double.valueOf(line[i]));
//          } else if (attributes[i].getType().getSimpleName().equals("UUID")) {
//            // TODO review, it not not work
//            // System.out.println("UUID " + line[i]);
//            attributes[i].set(object, UUID.fromString(line[i]));
//          } else {
//            attributes[i].set(object, line[i]);
//          }
//        }
//        result.add(cloneObject(object));
//        log.debug("Object {}", object);
//        index++;
//      }
//    } catch (IOException | IllegalArgumentException | IllegalAccessException e) {
//      e.printStackTrace();
//    }
//    return result;
//  }
//
//  private static Object cloneObject(Object obj) {
//    try {
//      Object clone = obj.getClass().newInstance();
//      for (Field field : obj.getClass().getDeclaredFields()) {
//        field.setAccessible(true);
//        if (field.get(obj) == null || Modifier.isFinal(field.getModifiers())) {
//          continue;
//        }
//        if (field.getType().isPrimitive() || field.getType().equals(String.class)
//            || field.getType().getSuperclass().equals(Number.class)
//            || field.getType().equals(Boolean.class)) {
//          field.set(clone, field.get(obj));
//        } else {
//          Object childObj = field.get(obj);
//          if (childObj == obj) {
//            field.set(clone, clone);
//          } else {
//            field.set(clone, cloneObject(field.get(obj)));
//          }
//        }
//      }
//      return clone;
//    } catch (Exception e) {
//      return null;
//    }
//  }
//
//}
