package es.bcg.tfm.migraine.controller.model.v1.migraine.response

data class MinimalMigraineResponse(

        var id: Int,

        var type: String,

        var startDate: String,

        var startTime: String

)