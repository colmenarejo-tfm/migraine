package es.bcg.tfm.migraine.controller.model.v1.study.response

data class StudiesResponse(val studies: List<StudyResponse>)