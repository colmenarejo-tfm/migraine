package es.bcg.tfm.migraine.controller.model.v1.patient.response

data class MinimalPatientsResponse(val patients: List<MinimalPatientResponse>)