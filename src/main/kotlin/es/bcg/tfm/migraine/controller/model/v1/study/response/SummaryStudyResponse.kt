package es.bcg.tfm.migraine.controller.model.v1.study.response

data class SummaryStudyResponse (

        var weeks : MutableList<SummaryWeekResponse> = mutableListOf(),

        var allPatients : MutableList<String> = mutableListOf()

)