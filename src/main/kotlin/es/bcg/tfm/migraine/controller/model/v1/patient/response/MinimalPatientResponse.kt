package es.bcg.tfm.migraine.controller.model.v1.patient.response

data class MinimalPatientResponse (

        var id: String,

        var email: String,

        var name: String? = null,

        var sex: String,

        var startDate: String

)