package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.service.PatientService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


@Api(tags = ["Migraine"])
@RestController
class MigraineController @Autowired constructor(private val service: PatientService) {

    /**
     * Get a migraine for a patient in a study.
     *
     * @param id path param - migraine id.
     * @param patientId query param - patient id.
     * @param studyId query param - study id.
     * @return a migraine for a patient in a study.
     */
    @ApiOperation(value = "Get a migraine for a patient in a study")
    @RequestMapping(value = ["/migraines/{id}"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getMigraine(@PathVariable(name = "id", required = true, value = "id") id: Int,
                    @RequestParam(required = true) patientId: String,
                    @RequestParam(required = true) studyId: Int): MigraineResponse {
        return service.getMigrainesForPatient(studyId, patientId, id)
    }

}