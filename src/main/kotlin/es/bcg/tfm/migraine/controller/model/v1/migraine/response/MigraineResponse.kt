package es.bcg.tfm.migraine.controller.model.v1.migraine.response

import com.fasterxml.jackson.annotation.JsonIgnore

data class MigraineResponse (

        var studyId: Int,

        var patientId: String,

        var migraineId: Int,

        var type : String,

        var aura : String = String(),

        var painIntensity: Int = 0,

        var evolPainPoints: Int = 0,

        @JsonIgnore
        var premonitory: List<String> = mutableListOf(),

        var symtoms : MutableList<String> = mutableListOf(),

        var medicines : MutableList<String> = mutableListOf()

)