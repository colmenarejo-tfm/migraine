package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.service.PatientService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Patients"])
@RestController
class PatientController @Autowired constructor(private val service: PatientService) {

    /**
     * Get all minimal patients for a study
     *
     * @param studyId query param - study id.
     * @return all minimal patients for a study.
     */
    @ApiOperation(value = "Get all minimal patients for a study")
    @RequestMapping(value = ["/patients"], method = [GET], produces = ["application/json"])
    fun getPatients(@RequestParam(required = true) studyId: Int): MinimalPatientsResponse {
        return service.getPatients(studyId)
    }

    /**
     * Get a complete patient for a study (include sociodemographical data and info for migraines).
     *
     * @param id path param - patient id.
     * @param studyId query param - study id.
     * @return a complete patient for a study (include sociodemographical data and info for migraines).
     */
    @ApiOperation(value = "Get a complete patient for a study (include sociodemographical data and info for migraines)")
    @RequestMapping(value = ["/patients/{id}"], method = [GET])
    fun getPatient(@PathVariable(name = "id", required = true, value = "id") id: String,
                   @RequestParam(required = true) studyId: Int): PatientResponse {
        return service.getPatient(studyId, id)
    }

//    @RequestMapping(value = ["/patients/{id}"], method = [POST])
//    fun savePersonalInfo(@PathVariable(name = "id", required = true, value = "id") id : String,
//                         @NotNull @RequestBody(
//                                 required = true) @Valid request : PatientPersonalInformationRequest) : PatientResponse? {
//        return service.postPatient(id, request)
//    }

}
