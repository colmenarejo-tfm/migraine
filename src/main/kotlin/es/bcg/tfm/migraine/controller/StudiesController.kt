package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.SummaryStudyResponse
import es.bcg.tfm.migraine.service.StudiesService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Studies"])
@RestController
class StudiesController @Autowired constructor(private val service: StudiesService) {

    /**
     * Get all studies.
     *
     * @return all studies.
     */
    @ApiOperation(value = "Get all studies")
    @RequestMapping(value = ["/studies"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getStudies(): StudiesResponse {
        return service.getStudies()
    }

    /**
     * Get a summary study.
     *
     * @param id path param - study id.
     * @return a summary for a study.
     */
    @ApiOperation(value = "Get a summary for a study")
    @RequestMapping(value = ["/studies/{id}"], method = [RequestMethod.GET])
    fun getSummary(@PathVariable(name = "id", required = true, value = "id") id: Int): SummaryStudyResponse {
        return service.getSummary(id)
    }

}