package es.bcg.tfm.migraine.controller.model.v1.study.response

data class SummaryWeekResponse (

        var id : Int = 0,

        var activePatients : Int = 0,

        var basalDaysExpected : Int = 0,

        var migrainesExpected : Int = 0,

        // List patient id active
        var patients : MutableList<String> = mutableListOf()

)