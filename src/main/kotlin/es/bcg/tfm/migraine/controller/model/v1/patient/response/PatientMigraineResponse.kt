package es.bcg.tfm.migraine.controller.model.v1.patient.response

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MinimalMigraineResponse

data class PatientMigraineResponse(

        var total: Integer,

        var toPredict: Integer,

        var falses: Integer,

        var notMonitored: Integer,

        var withNoise: Integer,

        var basalDays: Integer,

        var migraines: List<MinimalMigraineResponse>?

)