package es.bcg.tfm.migraine.controller.model.v1.patient.response

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MinimalMigraineResponse

data class PatientResponse(

        var id: String,

        var email: String,

        var name: String? = null,

        var sex: String,

        var startDate: String,

        var sociodemo: PatientSociodemoResponse?,

        val migraines: List<MinimalMigraineResponse>
) {

    constructor() : this(String(), String(), null,  String(), String(), null, mutableListOf())

}