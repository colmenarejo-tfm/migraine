package es.bcg.tfm.migraine.security.service


// TODO uncomment annotation
// @FunctionalInterface
interface TokenService {

    fun getToken(user: String, role: String): String

}