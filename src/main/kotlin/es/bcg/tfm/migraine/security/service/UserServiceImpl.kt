package es.bcg.tfm.migraine.security.service

// TODO uncomment
//package es.bcg.tfm.migraine.service
//
//import es.bcg.tfm.migraine.security.jpa.entity.UserDto
//import es.bcg.tfm.migraine.security.exception.InvalidUserException
//import es.bcg.tfm.migraine.deleted.controller.request.UserRequest
//import es.bcg.tfm.migraine.security.jpa.repository.RoleRepository
//import es.bcg.tfm.migraine.security.jpa.repository.UserRepository
//import mu.KotlinLogging
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.security.crypto.password.PasswordEncoder
//import org.springframework.stereotype.Service
//
//// TODO uncomment annotation
//// @Service
//class UserServiceImpl : UserService {
//
//    private val log = KotlinLogging.logger {}
//
//    private val userRepository : UserRepository
//
//    private val roleRepository : RoleRepository
//
//    @Autowired
//    private lateinit var passwordEncoder: PasswordEncoder
//
//    @Autowired
//    constructor(userRepository : UserRepository, roleRepository : RoleRepository) {
//        this.userRepository = userRepository
//        this.roleRepository = roleRepository
//    }
//
//    override fun addUser(request: UserRequest) {
//        if (!roleRepository.findById(request.role).isPresent) {
//            throw InvalidUserException("Invalid role")
//        }
//
//        if(userRepository.findByUsername(request.user) != null) {
//            throw InvalidUserException("User exist yet")
//        }
//
//        val user = UserDto(username = request.user, password = passwordEncoder.encode(request.password), role = request.role)
//        userRepository.save(user)
//    }
//
//}