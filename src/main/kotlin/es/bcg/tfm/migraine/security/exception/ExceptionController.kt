package es.bcg.tfm.migraine.security.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * POC this class is useless
 */
// @ControllerAdvice
class ExceptionController {

    @ExceptionHandler(NotFoundTokenOnHeaderException::class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    fun notFoundFileExceptionHandling(exception: NotFoundTokenOnHeaderException): ErrorResponse {
        return ErrorResponse(HttpStatus.FORBIDDEN.toString(), exception.message)
    }

}