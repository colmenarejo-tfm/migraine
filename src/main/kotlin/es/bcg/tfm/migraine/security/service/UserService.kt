package es.bcg.tfm.migraine.security.service

import es.bcg.tfm.migraine.deleted.controller.request.UserRequest

interface UserService {

    fun addUser(request : UserRequest)

}