package es.bcg.tfm.migraine.security.service

// TODO uncomment
//package es.bcg.tfm.migraine.security.service
//
//import es.bcg.tfm.migraine.security.jpa.repository.UserRepository
//import es.bcg.tfm.migraine.security.model.TokenUserDetails
//import es.bcg.tfm.migraine.security.service.TokenService
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.data.repository.NoRepositoryBean
//import org.springframework.security.core.authority.SimpleGrantedAuthority
//import org.springframework.security.core.userdetails.UserDetails
//import org.springframework.security.core.userdetails.UserDetailsService
//import org.springframework.security.core.userdetails.UsernameNotFoundException
//import org.springframework.stereotype.Service
//import java.util.*
//
//// TODO uncomment annotation
//// @Service
//class UserDetailsServiceImpl : UserDetailsService {
//
//    private val userRepository : UserRepository
//
//    private val tokenService : TokenService
//
//    @Autowired
//    constructor(userRepository: UserRepository, tokenService: TokenService){
//        this.userRepository = userRepository
//        this.tokenService = tokenService
//    }
//
//    @Throws(UsernameNotFoundException::class)
//    override fun loadUserByUsername(username: String): UserDetails {
//        val user = userRepository.findByUsername(username)
//                ?: throw UsernameNotFoundException("User not found in database")
//
//        return TokenUserDetails(user.username, user.password, getAuthorities(user.role),
//                tokenService.getToken(user.username, user.role))
//    }
//
//    private fun getAuthorities(role : String): List<SimpleGrantedAuthority> {
//        val roles: MutableList<SimpleGrantedAuthority> = ArrayList()
//        roles.add(SimpleGrantedAuthority(role))
//        return roles
//    }
//}
