package es.bcg.tfm.migraine.security.controller

import es.bcg.tfm.migraine.security.exception.ErrorResponse
import es.bcg.tfm.migraine.security.exception.InvalidUserException
import es.bcg.tfm.migraine.deleted.controller.request.UserRequest
import es.bcg.tfm.migraine.security.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Api(tags = ["User"])
// TODO uncomment annotation
// @RestController
class UserController {

    private val service : UserService

    @Autowired
    constructor(service : UserService){
        this.service = service
    }

    @ApiOperation(value = "Create new user")
    @RequestMapping(value = ["/users"], method = [RequestMethod.POST], consumes = ["application/json"])
    fun login(@Valid @NotNull @RequestBody(required = true) request: UserRequest) {
        service.addUser(request)
    }

    @ExceptionHandler(InvalidUserException::class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    fun notFoundFileExceptionHandling(exception: InvalidUserException): ErrorResponse {
        return ErrorResponse(HttpStatus.UNAUTHORIZED.toString(), exception.message)
    }

}