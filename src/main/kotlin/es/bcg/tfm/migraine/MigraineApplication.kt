package es.bcg.tfm.migraine

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MigraineApplication

    fun main(args: Array<String>) {
        runApplication<MigraineApplication>(*args)
    }

