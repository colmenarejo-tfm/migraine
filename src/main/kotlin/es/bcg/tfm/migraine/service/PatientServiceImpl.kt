package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MinimalMigraineResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientSociodemoResponse
import es.bcg.tfm.migraine.controller.model.v1.request.PatientPersonalInformationRequest
import es.bcg.tfm.migraine.jpa.entity.E4SessionsDto
import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.projection.PatientProjection
import es.bcg.tfm.migraine.jpa.repositoty.*
import es.bcg.tfm.migraine.util.DateFormatUtil.Companion.formatDate
import es.bcg.tfm.migraine.util.DateFormatUtil.Companion.formatTime
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
class PatientServiceImpl @Autowired constructor(private val patientRepository: PatientRepository,
                                                private val migraineRepository: MigraineRepository,
                                                private val e4SessionsRepository: E4SessionsRepository,
                                                private val evolRepository: EvolRepository,
                                                private val symtonsRepository: SymptomsRepository,
                                                private val medicinesRepository: MedicinesRepository) : PatientService {

    companion object {
        const val GOOD_TYPE = "good"
    }

    private val log = KotlinLogging.logger {}

    private val mapperSocio = { patient: PatientProjection ->
        PatientSociodemoResponse(patient.phone, formatDate.format(patient.birthdayDate), formatDate.format(patient.endDate),
                formatDate.format(patient.studyStartDate), patient.cognitiveReserve, patient.studyLevel, patient.employment,
                patient.diagnosis, patient.crisisFrequency, patient.background, patient.startAge, patient.hypertension,
                patient.diabetes, patient.dyslipidemia, patient.smoker, patient.alcohol, patient.painQuality,
                patient.menstruation, patient.coffe, patient.stress, patient.anxiety, patient.depression,
                patient.sleepDisturbance, patient.imc, patient.hit
        )
    }

    /*
    private val mapperMigraine = { migraine: MigraineProjection ->
        MinimalMigraineResponse(migraine.migraineId, migraine.calculateType(), formatDate.format(migraine.sessionStartTime),
                formatTime.format(migraine.sessionStartTime))
    }
    */

    override fun getPatients(studyId: Int): MinimalPatientsResponse {
        var patientsProjection: List<PatientProjection> = mutableListOf()
        try {
            patientsProjection = patientRepository.findAllPatientsByStudy(studyId) as List
        } catch (e: Exception) {
            val ids = patientRepository.findAllPatientsIdByStudy(studyId)
            if (!ids.isEmpty()) {
                patientsProjection = mutableListOf()
                for (id in ids) {
                    try {
                        val patient = patientRepository.findPatientByIdAndStudyId(id, studyId)
                        if (patient.isPresent) {
                            patientsProjection.add(patient.get())
                        }
                    } catch (e: Exception) {
                        log.debug("Error in patient whit id \"${id}\" : ${e}")
                    }

                }
            }
        }
        return MinimalPatientsResponse(patientsProjection.stream().map { patient ->
            MinimalPatientResponse(patient.id,
                    patient.user, patient.fullName(),
                    patient.sex, formatDate.format(patient.startDate))
        }.collect(Collectors.toList()))
    }


    override fun getPatient(studyId: Int, id: String): PatientResponse {

        val optional = patientRepository.findPatientByIdAndStudyId(id, studyId)

        if (optional.isPresent) {

            var patient = optional.get()

            // Get all migraines for a patient
            val migraines = migraineRepository.findAllByPatientId(id)

            // Get all sessions for a patient
            val sessions = e4SessionsRepository.findAllByPatientId(id)

            return PatientResponse(patient.id, patient.user, patient.fullName(), patient.sex,
                    formatDate.format(patient.startDate), mapperSocio(patient),
                    calculateMigraines(migraines, sessions))
        }

        return PatientResponse()
    }

    override fun getMigrainesForPatient(studyId: Int, id: String, migraineId: Int): MigraineResponse {

        // Check patient exist in this study.
        val optional = patientRepository.findPatientByIdAndStudyId(id, studyId)

        // Initialize migraine
        val response = MigraineResponse(studyId, id, migraineId, "NOT_MONITORED")

        if (optional.isPresent) {
            val migraineOptional = migraineRepository.findOneByMigraineIdAndPatientId(migraineId, id)

            if (migraineOptional.isPresent) {

                val migraine = migraineOptional.get()

                // Calculate aura
                if (migraine.auraStartTime != null) {
                    if (migraine.auraVisual != null && migraine.auraVisual == true) {
                        response.aura += "aura visual, "
                    }

                    if (migraine.auraLanguage != null && migraine.auraLanguage == true) {
                        response.aura += "aura de idioma, "
                    }

                    if (migraine.auraSensitive != null && migraine.auraSensitive == true) {
                        response.aura += "aura de sensitivo, "
                    }

                    if (migraine.auraOthers != null && migraine.auraOthers == true) {
                        response.aura += "otro tipo de aura"
                    }

                    if (response.aura.endsWith(", ")) {
                        response.aura = response.aura.substring(0, response.aura.length - 2)
                    }
                    response.aura.capitalize()
                }

                // Calculate pain intensity
                response.painIntensity = migraine.painIntensity ?: 0

                // Calculate evol pain intensity
                val evols = evolRepository.findAllByPatientIdAndMigraineId(id, migraineId)
                for (evol in evols) {
                    response.evolPainPoints += evol.evolValue
                }

                // Calculate symtons
                val symtons = symtonsRepository.findAllByPatientIdAndMigraineId(id, migraineId)
                for (symton in symtons) {
                    response.symtoms.add(symton.name())
                }

                // Calculate medicines
                val medicines = medicinesRepository.findAllByPatientIdAndMigraineId(id, migraineId)
                for (medicine in medicines) {
                    response.medicines.add(medicine.name())
                }

                // Calculate type
                val sessions = e4SessionsRepository.findAllByPatientId(id)

                val migraines = mutableListOf<MigraineDto>()
                migraines.add(migraine)

                val minimalResponses = calculateMigraines(migraines, sessions)

                response.type = minimalResponses[0].type
            }
        }

        return response
    }


    override fun postPatient(id: String, personalInfo: PatientPersonalInformationRequest): PatientResponse? {
        val patientOptional = patientRepository.findById(id)

//        if (patientOptional.isPresent){
//            var patient = patientOptional.get()
//            patient.name = personalInfo.name
//            patient.surname1 = personalInfo.surname
//            patient.surname2 = personalInfo.secondSurname
//            patient.phone = personalInfo.phone
//            patient.comments = personalInfo.comment
//
//            patientRepository.save(patient)
//            return mapper(patient)
//        }
        return null
    }


    private fun calculateMigraines(migraines: List<MigraineDto>, sessions: List<E4SessionsDto>): List<MinimalMigraineResponse> {

        var mSession = hashMapOf<MigraineDto, E4SessionsDto>()

        var response = mutableListOf<MinimalMigraineResponse>()

        for (migraine in migraines) {
            if (migraine.painStartTime == null) {
                log.debug("Skip migraine ${migraine.migraineId} for patient ${migraine.patientId} " +
                        ": pain start time is null")

                response.add(MinimalMigraineResponse(migraine.migraineId, "NOT_MONITORED",
                        String(), String()))

                continue
            }

            val startPain = migraine.painStartTime
            val migraineDay = formatDate.format(migraine.painStartTime)

            var count = 0

            for (session in sessions) {
                val startSession = session.startTime
                val sessionDay = formatDate.format(session.startTime)

                if (migraineDay == sessionDay && formatTime.parse(formatTime.format(startSession)).before(
                                formatTime.parse(formatTime.format(startPain)))) {

                    if (count > 0) {
                        log.error("Multiple session for a some migraine exist, review migraine" +
                                " ${migraine.migraineId} for patient ${migraine.patientId}")
                    }
                    // Attention this is a map --> mSession.put(migraine, session)
                    mSession[migraine] = session

                    count += 1
                }
            }

            if (count == 0) {
                log.debug("Skip migraine ${migraine.migraineId} for patient ${migraine.patientId} " +
                        ": Not match with session")

                response.add(MinimalMigraineResponse(migraine.migraineId, "NOT_MONITORED",
                        String(), String()))
            }
        }

        for ((m, s) in mSession) {
            response.add(MinimalMigraineResponse(m.migraineId, calculateMigraineType(m, s),
                    formatDate.format(s.startTime), formatTime.format(s.startTime)))
        }

        return response.sortedWith(compareBy { it.id })
    }

    private fun calculateMigraineType(migraine: MigraineDto, session: E4SessionsDto): String {

        val startSession = session.startTime
        val startPain = session.startTime

        if (migraine.falseEpisodeTime != null) {

            return "FALSE"

        } else if (session.startTime.before(session.startTime) && (startSession.time - startPain.time >= 60)) {

            if (session.tempQuality == GOOD_TYPE && session.edaQuality == GOOD_TYPE && session.hrQuality == GOOD_TYPE) {

                return "GOOD"

            }

            return "WITH_NOISE"

        }

        return "NOT_MONITORED"

    }

}