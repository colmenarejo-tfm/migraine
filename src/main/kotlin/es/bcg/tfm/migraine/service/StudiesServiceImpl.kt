package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.StudyResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.SummaryStudyResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.SummaryWeekResponse
import es.bcg.tfm.migraine.jpa.entity.StudiesDto
import es.bcg.tfm.migraine.jpa.repositoty.PatientRepository
import es.bcg.tfm.migraine.jpa.repositoty.StudiesRepository
import es.bcg.tfm.migraine.util.DateFormatUtil.Companion.formatDate
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*


@Service
class StudiesServiceImpl @Autowired constructor(private val studiesRepository: StudiesRepository,
                                                private val patientRepository: PatientRepository) : StudiesService {

    private val log = KotlinLogging.logger {}

    private val mapper = { study: StudiesDto -> StudyResponse(study.id, study.name, formatDate.format(study.startDate)) }


    override fun getStudies(): StudiesResponse {

        val studies = studiesRepository.findAll()

        val studiesResponse: MutableList<StudyResponse> = mutableListOf()
        for (study: StudiesDto in studies) {
            studiesResponse.add(mapper(study))
        }

        return StudiesResponse(studiesResponse)
    }

    override fun getSummary(id: Int): SummaryStudyResponse {

        var response = SummaryStudyResponse()

        // Get all patients in study
        var patients = patientRepository.findAllPatientsIdAndEndDateByStudy(id)
        patients.forEach { patient -> response.allPatients.add(patient.id) }

        // Get summary info by week
        val optional = studiesRepository.findById(id)

        if (optional.isPresent) {
            val study = optional.get()

            // Study day start
            val localDateStudy = transformDateInLocalDate(study.startDate?.time ?: Date().time)

            // Now
            val localDateNow = LocalDate.now()

            // Calculate week start month
            var localDateWeekMonth = LocalDate.now().minusDays(localDateNow.dayOfMonth.toLong())
            val weekStartMonth = calculateWeek(localDateWeekMonth, localDateStudy)

            // Calculate current week
            val currentWeek = calculateWeek(localDateNow, localDateStudy)

            // Iterate for all weeks in month
            for (i in weekStartMonth..currentWeek) {

                // Create summary week
                var summaryWeek = SummaryWeekResponse()

                // Id week
                summaryWeek.id = i

                // Calculate active patients
                var count = 0
                for (patient in patients) {
                    if (patient.endDate == null) {
                        summaryWeek.patients.add(patient.id)
                        count += 1
                    } else {
                        val endStudy = transformDateInLocalDate(patient.endDate!!.time)

                        if (i <= calculateWeek(endStudy, localDateStudy)) {
                            summaryWeek.patients.add(patient.id)
                            count += 1
                        }
                    }
                }

                summaryWeek.activePatients = count

                // Calculate basal days expected
                summaryWeek.basalDaysExpected = count * 4

                // Calculate migraines expected
                summaryWeek.migrainesExpected = count * 3

                // Add week in final response
                response.weeks.add(summaryWeek)
            }
        }

        return response
    }

    private fun transformDateInLocalDate(date: Long): LocalDate {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(date), ZoneId.systemDefault()).toLocalDate()
    }

    private fun calculateWeek(new: LocalDate, old: LocalDate): Int {
        return ((((new.year - old.year) * 365) + new.dayOfYear) - old.dayOfYear) / 7
    }
}