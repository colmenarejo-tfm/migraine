package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.SummaryStudyResponse

interface StudiesService {

    /**
     * Get all studies.
     *
     * @return all studies.
     */
    fun getStudies(): StudiesResponse

    /**
     * Get a summary study.
     *
     * @param id study id.
     * @return a summary for a study.
     */
    fun getSummary(id: Int): SummaryStudyResponse

}