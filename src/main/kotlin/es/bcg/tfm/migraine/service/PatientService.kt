package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.controller.model.v1.request.PatientPersonalInformationRequest

interface PatientService {

    /**
     * Get minimal information for patients for a study.
     *
     * @param studyId study id.
     * @return minimal information for patients for a study.
     */
    fun getPatients(studyId: Int): MinimalPatientsResponse

    /**
     * Get complete information for a patients for a study.
     *
     * @param studyId study id.
     * @param id patient id.
     * @return complete information for a patients for a study.
     */
    fun getPatient(studyId: Int, id: String): PatientResponse

    /**
     * Get migraine info for a patient in study.
     *
     * @param studyId study id.
     * @param migraineId migraine id.
     * @param id patient id.
     * @return migraine info for a patient.
     */
    fun getMigrainesForPatient(studyId: Int, id: String, migraineId: Int): MigraineResponse


    fun postPatient(id: String, personalInfo: PatientPersonalInformationRequest): PatientResponse?

}