package es.bcg.tfm.migraine.jpa.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "medicines_info")
data class MedicinesInfoDto(

        @Id
        @Column(name = "medicine_id", nullable = false, length = 5)
        val id: Int,

        @Column(name = "name_es", nullable = false)
        val name: String

)
