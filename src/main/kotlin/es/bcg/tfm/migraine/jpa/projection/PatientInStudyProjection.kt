package es.bcg.tfm.migraine.jpa.projection

import java.util.*

data class PatientInStudyProjection(

        var id: String,

        var endDate: Date? = null

)