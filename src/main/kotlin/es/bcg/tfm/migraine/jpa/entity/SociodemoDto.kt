package es.bcg.tfm.migraine.jpa.entity

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity(name = "sociodemo")
data class SociodemoDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9)
        var patientId: String,

        @Column(nullable = false)
        var studyStartDate: Date,

        @Column(nullable = false)
        var birthdayDate: Date,

        @Column(nullable = false, columnDefinition = "enum('H','M')")
        var sex: String,

        @Column(nullable = false, length = 11)
        var cognitiveReserve: Int,

        @Column(nullable = false, length = 20)
        var studyLevel: String,

        @Column(nullable = false, length = 20)
        var employment: String,

        @Column(nullable = false, length = 40)
        var diagnosis: String,

        @Column(nullable = false, length = 50)
        var crisisFrequency: String,

        @Column(nullable = false)
        var background: Boolean,

        @Column(nullable = false, length = 11)
        var startAge: Int,

        @Column(nullable = false)
        var hypertension: Boolean,

        @Column(nullable = false)
        var diabetes: Boolean,

        @Column(nullable = false)
        var dyslipidemia: Boolean,

        @Column(nullable = false)
        var smoker: Boolean,

        @Column(nullable = false, length = 20)
        var alcohol: String,

        @Column(nullable = false, length = 20)
        var painQuality: String,

        @Column(nullable = false)
        var menstruation: Boolean,

        @Column(nullable = false)
        var coffe: Boolean,

        @Column(nullable = false)
        var stress: Boolean,

        @Column(nullable = false)
        var anxiety: Boolean,

        @Column(nullable = false)
        var depression: Boolean,

        @Column(nullable = false, length = 40)
        var sleepDisturbance: String,

        @Column(nullable = false)
        var imc: BigDecimal,

        @Column(nullable = false, length = 11)
        var hit: Int,

        @OneToOne
        @JoinColumn(name = "patient_id", referencedColumnName = "patient_id")
        var patient: PatientDto
)
