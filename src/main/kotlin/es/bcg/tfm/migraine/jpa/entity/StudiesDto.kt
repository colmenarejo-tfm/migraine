package es.bcg.tfm.migraine.jpa.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "studies")
data class StudiesDto(

        @Id
        @Column(name = "study_id", nullable = false, length = 5)
        var id: Int,

        @Column(nullable = false, length = 255)
        var name: String,

        var startDate: Date? = null,

        var endDate: Date? = null,

        @Column(columnDefinition = "enum('case series','medical records'," +
                "'prevalence and incidence','cohort','cases and controls','natural experiments'," +
                "'clinicalessay','meta-analysis')")
        var type: String? = null,

        @Column(columnDefinition = "enum('unicentric','multicentric')")
        var size: String? = null,

        @Column(length = 2047)
        var description: String? = null,

        @Column(length = 2047)
        var comments: String? = null,

        @Column(length = 255)
        var fundingEntity: String? = null,

        @Column(length = 255)
        var fundingCode: String? = null
)

