package es.bcg.tfm.migraine.jpa.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity(name = "migraines")
@IdClass(MigraineId::class)
data class MigraineDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9, insertable = false, updatable = false)
        val patientId: String,

        @Id
        @Column(name = "migraine_id", nullable = false, length = 5)
        val migraineId: Int,

        @JsonIgnore
        var triggerFood: Boolean? = null,

        @JsonIgnore
        var triggerDrink: Boolean? = null,

        @JsonIgnore
        var triggerStress: Boolean? = null,

        @JsonIgnore
        var triggerSleep: Boolean? = null,

        @JsonIgnore
        var triggerWeather: Boolean? = null,

        @JsonIgnore
        var triggerSports: Boolean? = null,

        @JsonIgnore
        var triggerOthers: Boolean? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var falseEpisodeTime: Date? = null,

        @JsonIgnore
        @Temporal(TemporalType.TIMESTAMP)
        var auraStartTime: Date? = null,

        @JsonIgnore
        @Temporal(TemporalType.TIMESTAMP)
        var auraEndTime: Date? = null,

        @JsonIgnore
        var auraVisual: Boolean? = null,

        @JsonIgnore
        var auraLanguage: Boolean? = null,

        @JsonIgnore
        var auraSensitive: Boolean? = null,

        @JsonIgnore
        var auraOthers: Boolean? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var painStartTime: Date? = null,

        @Temporal(TemporalType.TIMESTAMP)
        var painEndTime: Date? = null,

        @JsonIgnore
        @Column(columnDefinition = "enum('Holocraneal','Hemicraneal','Parte concreta','')")
        var painInitZone: String? = null,

        @JsonIgnore
        var painIrradiation: Boolean? = null,

        @JsonIgnore
        @Column(length = 9)
        var painIntensity: Int? = null,

        @JsonIgnore
        @Column(columnDefinition = "enum('Pulsátil','Opresivo','','')")
        var painQuality: String? = null,

        @JsonIgnore
        @Column(columnDefinition = "enum('Durmiendo','Al despertar','Haciendo deporte o esfuerzo físico'," +
                "'Tras hacer deporte o esfuerzo físico','Trabajando o estudiando'," +
                "'Haciendo una actividad de ocio relajante','Comiendo','Otros')")
        var painInitActivity: String? = null,

        @JsonIgnore
        var painInterruption: Boolean? = null,

        @JsonIgnore
        var painImprovementFactors: Boolean? = null,

        @JsonIgnore
        var painSimulNauseas: Boolean? = null,

        @JsonIgnore
        var painSimulVomits: Boolean? = null,

        @JsonIgnore
        var painSimulPhoto: Boolean? = null,

        @JsonIgnore
        var painSimulSono: Boolean? = null,

        @JsonIgnore
        var painSimulOsmo: Boolean? = null,

        @JsonIgnore
        var painSimulOthers: Boolean? = null,

        @JsonIgnore
        var painPostMood: Boolean? = null,

        @JsonIgnore
        var painPostApetite: Boolean? = null,

        @JsonIgnore
        var painPostApetiteType: Boolean? = null,

        @JsonIgnore
        var painPostCognitive: Boolean? = null,

        @JsonIgnore
        var painPostSleep: Boolean? = null,

        @JsonIgnore
        var painPostTemp: Boolean? = null,

        @JsonIgnore
        var painPostYawn: Boolean? = null,

        @JsonIgnore
        var painPostOthers: Boolean? = null
)

data class MigraineId(
        var patientId: String = "",

        var migraineId: Int = 0
) : Serializable