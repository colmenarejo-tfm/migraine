package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.EvolDto
import org.springframework.data.jpa.repository.JpaRepository

interface EvolRepository : JpaRepository<EvolDto, EvolDto> {

    fun findAllByPatientIdAndMigraineId(patientId: String, migraineId: Int) : List<EvolDto>

}