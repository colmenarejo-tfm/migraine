package es.bcg.tfm.migraine.jpa.projection

import java.util.*

data class MigraineProjection (

        val patientId: String,

        val migraineId: Int,

        val sessionId: String,

        val sessionStartTime: Date?,

        val sessionEndTime: Date?,

        val temp: String?,

        val eda: String?,

        val hr: String?,

        val SymptomStartTime: Date?,

        var painStartTime: Date? = null,

        var painEndTime: Date? = null,

        var falseEpisodeTime: Date? = null
)