package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.MedicinesDto
import es.bcg.tfm.migraine.jpa.entity.MedicinesId
import org.springframework.data.jpa.repository.JpaRepository

interface MedicinesRepository : JpaRepository<MedicinesDto, MedicinesId> {

    fun findAllByPatientIdAndMigraineId(patientId: String, migraineId: Int) : List<MedicinesDto>

}