package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.E4SessionsDto
import es.bcg.tfm.migraine.jpa.entity.E4SessionsId
import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.entity.MigraineId
import es.bcg.tfm.migraine.jpa.projection.MigraineProjection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface E4SessionsRepository : JpaRepository<E4SessionsDto, E4SessionsId> {

    fun findAllByPatientIdAndStartTimeIsBeforeAndEndTimeIsAfter(patientId: String, startTime : Date, endTime: Date) : List<E4SessionsDto>

    fun findAllByPatientId(patientId: String) : List<E4SessionsDto>

}