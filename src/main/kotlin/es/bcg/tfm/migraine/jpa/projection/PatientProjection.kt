package es.bcg.tfm.migraine.jpa.projection

import java.math.BigDecimal
import java.util.*

data class PatientProjection (

        var id: String,

        var user: String,

        var password: String,

        var startDate: Date,

        var endDate: Date? = null,

        var studyId: Int,

        var name: String? = null,

        var surname1: String? = null,

        var surname2: String? = null,

        var phone: String? = null,

        var points: Int = 0,

        var comments: String? = null,

        var studyStartDate: Date,

        var birthdayDate: Date,

        var sex: String,

        var cognitiveReserve: Int,

        var studyLevel: String,

        var employment: String,

        var diagnosis: String,

        var crisisFrequency: String,

        var background: Boolean,

        var startAge: Int,

        var hypertension: Boolean,

        var diabetes: Boolean,

        var dyslipidemia: Boolean,

        var smoker: Boolean,

        var alcohol: String,

        var painQuality: String,

        var menstruation: Boolean,

        var coffe: Boolean,

        var stress: Boolean,

        var anxiety: Boolean,

        var depression: Boolean,

        var sleepDisturbance: String,

        var imc: BigDecimal,

        var hit: Int
) {

    fun fullName() : String? {
        surname2 = surname2 ?: ""
        return "$name $surname1 $surname2".trim()
    }
}