package es.bcg.tfm.migraine.jpa.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity(name = "symptoms")
@IdClass(SymptonsId::class)
data class SymptomsDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9)
        val patientId: String,

        @Id
        @Column(name = "migraine_id", nullable = false, length = 11)
        val migraineId: Int,

        @Id
        @Column(name = "symptom_id", nullable = false, length = 5, updatable = false, insertable = false)
        val symptomId: Int,

        @Id
        @Column(nullable = false)
        @Temporal(TemporalType.TIMESTAMP)
        val symptomStartTime: Date,

        @Temporal(TemporalType.TIMESTAMP)
        val symptomEndTime: Date? = null,

        @OneToOne
        @JoinColumn(name = "symptom_id", referencedColumnName = "symptom_id")
        val symptomsInfo: SymptomsInfoDto
) {

    fun name(): String {
        return symptomsInfo.name
    }

}

data class SymptonsId(
        val patientId: String = "",

        val migraineId: Int = 0,

        val symptomId: Int = 0,

        val symptomStartTime: Date = Date()
) : Serializable