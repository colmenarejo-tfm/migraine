package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.PatientDto
import es.bcg.tfm.migraine.jpa.projection.PatientInStudyProjection
import es.bcg.tfm.migraine.jpa.projection.PatientProjection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface PatientRepository : JpaRepository<PatientDto, String> {

    @Query("SELECT new es.bcg.tfm.migraine.jpa.projection.PatientProjection(p.id, p.user, p.password, " +
            "p.startDate, p.endDate, p.studyId, p.name, p.surname1, p.surname2, p.phone, p.points, p.comments, " +
            "s.studyStartDate, s.birthdayDate, s.sex, s.cognitiveReserve, s.studyLevel, s.employment, s.diagnosis, " +
            "s.crisisFrequency, s.background, s.startAge, s.hypertension, s.diabetes, s.dyslipidemia, s.smoker, " +
            "s.alcohol, s.painQuality, s.menstruation, s.coffe, s.stress, s.anxiety, s.depression, s.sleepDisturbance, " +
            "s.imc, s.hit) FROM patients AS p, sociodemo AS s WHERE p.id = s.id AND p.studyId = :studyId")
    fun findAllPatientsByStudy(studyId: Int): List<PatientProjection>?

    @Query("SELECT new es.bcg.tfm.migraine.jpa.projection.PatientProjection(p.id, p.user, p.password, " +
            "p.startDate, p.endDate, p.studyId, p.name, p.surname1, p.surname2, p.phone, p.points, p.comments, " +
            "s.studyStartDate, s.birthdayDate, s.sex, s.cognitiveReserve, s.studyLevel, s.employment, s.diagnosis, " +
            "s.crisisFrequency, s.background, s.startAge, s.hypertension, s.diabetes, s.dyslipidemia, s.smoker, " +
            "s.alcohol, s.painQuality, s.menstruation, s.coffe, s.stress, s.anxiety, s.depression, s.sleepDisturbance, " +
            "s.imc, s.hit) FROM patients AS p, sociodemo AS s WHERE p.id = s.id AND p.id = :id AND p.studyId = :studyId")
    fun findPatientByIdAndStudyId(id: String, studyId: Int): Optional<PatientProjection>


    @Query("SELECT p.id FROM patients AS p, sociodemo AS s WHERE p.id = s.id AND p.studyId = :id")
    fun findAllPatientsIdByStudy(id: Int): Set<String>

    @Query("SELECT new es.bcg.tfm.migraine.jpa.projection.PatientInStudyProjection(p.id, p.endDate)" +
            "  FROM patients AS p, sociodemo AS s WHERE p.id = s.id AND p.studyId = :id")
    fun findAllPatientsIdAndEndDateByStudy(id: Int): List<PatientInStudyProjection>

}