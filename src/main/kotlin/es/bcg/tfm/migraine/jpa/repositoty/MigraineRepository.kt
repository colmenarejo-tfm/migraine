package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.entity.MigraineId
import es.bcg.tfm.migraine.jpa.projection.MigraineProjection
import es.bcg.tfm.migraine.jpa.projection.PatientProjection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.*

interface MigraineRepository : JpaRepository<MigraineDto, MigraineId> {

    @Query("SELECT DISTINCT new es.bcg.tfm.migraine.jpa.projection.MigraineProjection(e4.patientId, sy.migraineId," +
            " e4.sessionId, e4.startTime, e4.endTime," +
            " e4.tempQuality, e4.edaQuality, e4.hrQuality, sy.symptomStartTime," +
            " mi.painStartTime, mi.painEndTime, mi.falseEpisodeTime)" +
            " FROM e4_sessions AS e4, symptoms AS sy, migraines AS mi WHERE e4.patientId = sy.patientId" +
            " AND sy.migraineId = mi.migraineId AND e4.patientId = :patientId")
    fun findAllMigrainesByPatient(patientId: String): List<MigraineProjection>

    fun findAllByPatientId(patientId: String) : List<MigraineDto>

    fun findOneByMigraineIdAndPatientId(migraineId: Int, patientId: String) : Optional<MigraineDto>
}