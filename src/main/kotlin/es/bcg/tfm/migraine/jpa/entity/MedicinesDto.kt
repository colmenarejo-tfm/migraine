package es.bcg.tfm.migraine.jpa.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity(name = "medicines")
@IdClass(MedicinesId::class)
data class MedicinesDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9, insertable = false, updatable = false)
        var patientId: String = String(),

        @Id
        @Column(name = "migraine_id", nullable = false, length = 5)
        var migraineId: Int = 0,

        @Id
        @Temporal(TemporalType.TIMESTAMP)
        var medicineTime: Date? = null,

        @Id
        @Column(name = "medicine_id", nullable = false, insertable = false, updatable = false)
        var medicineId: Int = 0,

        @OneToOne
        @JoinColumn(name = "medicine_id", referencedColumnName = "medicine_id")
        val medicinesInfo: MedicinesInfoDto

) {

    fun name(): String {
        return medicinesInfo.name
    }

}


data class MedicinesId(
        var patientId: String = String(),

        var migraineId: Int = 0,

        var medicineTime: Date? = null,

        var medicineId: Int = 0
) : Serializable
