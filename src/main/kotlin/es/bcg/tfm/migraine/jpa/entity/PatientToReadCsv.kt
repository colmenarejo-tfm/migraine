package es.bcg.tfm.migraine.jpa.entity

data class PatientToReadCsv (

        var id : Int? = null,

        var uuid : String? = null,

        var email : String? = null,

        var startDate: String? = null,

        var date : String? = null,

        var sex : String? = null,

        var cognitiveReserve : Int? = null,

        var studies : String? = null,

        var employmentSituation : String? = null,

        var diagnosis : String? = null,

        var frequencyCrisis : String? = null,

        var background : Boolean? = null,

        var startAge : Int? = null,

        var hypertension : Boolean? = null,

        var diabetes : Boolean? = null,

        var displemia : Boolean? = null,

        var smoker : Boolean? = null,

        var alcohol : String? = null,

        var quality : String? = null,

        var menstruation : Boolean? = null,

        var coffee : Boolean? = null,

        var prolongedStress : Boolean? = null,

        var anxiety : Boolean? = null,

        var depression : Boolean? = null,

        var sleepDisturbance : Boolean? = null,

        var imc : Double? = null,

        var hit : Int? = null,

        var ttoGeneral : String? = null,

        var ttoPreventive : String? = null
)