package es.bcg.tfm.migraine.jpa.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity(name = "evol")
@IdClass(EvolId::class)
data class EvolDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9, insertable = false, updatable = false)
        var patientId: String = String(),

        @Id
        @Column(name = "migraine_id", nullable = false, length = 5)
        var migraineId: Int = 0,

        @Id
        @Temporal(TemporalType.TIMESTAMP)
        var evolTime: Date? = null,

        @Id
        var evolValue: Int = 0

)

data class EvolId(
        var patientId: String = String(),

        var migraineId: Int = 0,

        var evolTime: Date? = null,

        var evolValue: Int = 0
) : Serializable