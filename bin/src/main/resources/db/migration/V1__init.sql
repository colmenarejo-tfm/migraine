CREATE TABLE roles (
  name varchar (50) PRIMARY KEY,
  permits json NOT NULL
);

INSERT INTO roles(name, permits) VALUES ('admin', '{"all":true}');
INSERT INTO roles(name, permits) VALUES ('default', '{"all":false}');

CREATE TABLE users (
  id serial PRIMARY KEY,
  username varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  role varchar(50) NOT NULL REFERENCES roles(name)
);

CREATE TABLE medicine_info (
  id integer PRIMARY KEY,
  name varchar(50) NOT NULL
);

CREATE TABLE symptoms_info (
  id integer PRIMARY KEY,
  name varchar(50) NOT NULL
);

CREATE TABLE treatments(
  id integer PRIMARY KEY,
  name varchar(50) NOT NULL
);


CREATE TABLE studies (
  id serial PRIMARY KEY,
  name varchar(20) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  type enum('CLINICAL TRIAL','CLINICAL STUDY','PILOT'),
  size enum('UNI-CENTER','MULTI-CENTER'),
  description varchar(2000),
  comment varchar(2000),
  funding_entity varchar(50),
  funding_code varchar(50)
);

CREATE TABLE medical_centers (
  id serial PRIMARY KEY,
  name varchar(50),
  street varchar(255),
  number varchar(10),
  city varchar(20),
  zip_code varchar(5),
  logo mediumblob
);

CREATE TABLE studies_medical_centers (
  study_id integer references studies (id),
  medical_center_id integer references medical_centers(id),
  head varchar(255),
  start_date date,
  end_date date
);


CREATE TABLE patients (
  id serial PRIMARY KEY,
  uuid varchar(36) NOT NULL UNIQUE,
  email varchar(320) NOT NULL,
  start_date varchar(10) NOT NULL,
  date varchar(10) NOT NULL,
  sex varchar(1) NOT NULL,
  cognitive_reserve integer NOT NULL,
  studies enum('BASICS','PRIMARY','UNIVERSITY') NOT NULL,
  employment_situation enum('STUDENT','ACTIVE','UNEMPLOYED','OTHERS') NOT NULL,
  diagnosis enum('MIGRAINE WITHOUT AURA','MIGRAINE WITH AURA','TENSION HEADACHE','MIXED','TRIGEMINO-AUTONOMIC',
    'NEURALGIA','SECONDARY','HYPNICA','CERVICOGENIC','OTHERS') NOT NULL,
  frequency_crisis enum('UNCOMMON EPISODIC','EPISODIC','COMMON EPISODIC','CHRONICLE') NOT NULL,
  background boolean NOT NULL,
  start_age integer NOT NULL,
  hypertension boolean NOT NULL,
  diabetes boolean NOT NULL,
  displemia boolean NOT NULL,
  smoker boolean NOT NULL,
  alcohol enum('NEVER','SPORADICALLY','WEEKENDS','HABITUAL') NOT NULL,
  quality enum('IMPLOSIVE','EXPLOSIVE','RETROOCULAR','MIXED') NOT NULL,
  menstruation boolean NOT NULL,
  coffee boolean NOT NULL,
  prolonged_stress boolean NOT NULL,
  anxiety boolean NOT NULL,
  depression boolean NOT NULL,
  sleep_disturbance enum('NONE','INSOMNIA OF CONCILIATION','EARLY AWAKENING','DROWSINESS',
    'INSOMNIA AND EARLY AWAKENING','INSOMNIA AND DROWSINESS','EARLY AWAKENING AND DROWSINESS','ALL') NOT NULL,
  imc double NOT NULL,
  hit integer NOT NULL,
  tto_general varchar(255) NOT NULL,
  tto_preventive varchar(255) NOT NULL,
  name varchar(20),
  surname varchar(20),
  second_surname varchar(20),
  phone varchar(9),
  comment varchar(2000)
);

CREATE TABLE patients_treatments (
  patient_id integer REFERENCES patients(id),
  treatment_id integer REFERENCES treatments(id)
);

CREATE TABLE migraines (
  patient_id serial PRIMARY KEY,
  migraine_id integer NOT NULL,
  trigger_food boolean DEFAULT NULL,
  trigger_drink boolean DEFAULT NULL,
  trigger_stress boolean DEFAULT NULL,
  trigger_sleep boolean DEFAULT NULL,
  trigger_weather boolean DEFAULT NULL,
  trigger_sports boolean DEFAULT NULL,
  trigger_others boolean DEFAULT NULL,
  aura_init_time datetime DEFAULT NULL,
  aura_end_time datetime DEFAULT NULL,
  aura_visual boolean DEFAULT NULL,
  aura_language boolean DEFAULT NULL,
  aura_sensitive boolean DEFAULT NULL,
  aura_others boolean DEFAULT NULL,
  pain_init_time datetime DEFAULT NULL,
  pain_end_time datetime DEFAULT NULL,
  pain_init_zone enum('HOLOCRANIAL','HEMICRANIAL','CONCRETE PART','') DEFAULT NULL,
  pain_irradiation boolean DEFAULT NULL,
  pain_intensity integer DEFAULT NULL,
  pain_quality enum('PULSATILE','OPPRESSIVE','') DEFAULT NULL,
  pain_init_activity enum('SLEEPING','UPON AWAKENING','DOING SPORT OR PHYSICAL EFFORT',
    'AFTER DOING SPORT OR PHYSICAL EFFORT','WORKING OR STUDYING','DOING A RELAXING LEISURE ACTIVITY','EATING',
    'OTHERS') DEFAULT NULL,
  pain_interruption boolean DEFAULT NULL,
  pain_improvement_factors  boolean DEFAULT NULL,
  pain_simul_nauseas boolean DEFAULT NULL,
  pain_simul_vomits boolean DEFAULT NULL,
  pain_simul_photo boolean DEFAULT NULL,
  pain_simul_sono boolean DEFAULT NULL,
  pain_simul_osmo boolean DEFAULT NULL,
  pain_simul_others boolean DEFAULT NULL,
  pain_post_mood boolean DEFAULT NULL,
  pain_post_apetite boolean DEFAULT NULL,
  pain_post_cognitive boolean DEFAULT NULL,
  pain_post_sleep boolean DEFAULT NULL,
  pain_post_temp boolean DEFAULT NULL,
  pain_post_yawn boolean DEFAULT NULL,
  pain_post_neck boolean DEFAULT NULL,
  pain_post_others boolean DEFAULT NULL
);

CREATE TABLE evol (
  patient_id integer references patients (id),
  migraine_id integer references migraines (id),
  evol_time datetime NOT NULL,
  evol_value integer NOT NULL
);

CREATE TABLE medicines (
  patient_id integer references patients(id),
  migraine_id integer references migraines(id),
  medicine_time datetime NOT NULL,
  medicine enum('AINES','PARACETAMOL','TRIPTAN','PRIMPERAN','NOLOTIL','CODEINA','OTHERS') NOT NULL
);

CREATE TABLE symptoms (
  patient_id integer references patients(id),
  migraine_id integer references migraines(id),
  symptom_init_time datetime NOT NULL,
  symptom_end_time datetime NOT NULL,
  symptom enum('SADNESS','EUPHORIA','HYPERACTIVITY','ANXIETY','IRRITABILITY','APATHY','INCREASE APPETITE',
    'DECREASE APPETITE','APPEAL FOR SPECIFIC FOODS','INCREASE THIRST','INSOMNIA','DROWSINESS',
    'DIFFICULTY OF CONCENTRATION','DIFFICULTY TO EMIT LANGUAGE','DIFFICULTY IN WRITING','FEELING COLDER',
    'FEELING MORE HEAT','NAUSEA AND/OR VOMITING','FLATULENCE','CONSTIPATION','FLUID RETENTION / SWELLING',
    'INCREASE IN URINARY FREQUENCY','NUCHAL RIGIDITY','YAWNS','FATIGUE','SENSITIVITY TO LIGHT',
    'SENSITIVITY TO SOUNDS','SENSITIVITY TO ODORS','SENSITIVITY TO SKIN FRICTION','NOISE IN THE EAR',
    'BLURRY VISION','DIZZINESS','SKIN OR VASCULAR CHANGES','OTHERS') NOT NULL
);

CREATE VIEW migraines_data AS
SELECT e4.patient_id, sy.migraine_id, e4.session_id, e4.start_time, e4.end_time, e4.temp_quality,
       e4.eda_quality, e4.hr_quality, sy.symptom_start_time, mi.pain_start_time, mi.pain_end_time, mi.false_episode_time
FROM e4_sessions AS e4 JOIN symptoms AS sy ON e4.patient_id = sy.patient_id
                       JOIN migraines AS mi ON sy.migraine_id = mi.migraine_id;