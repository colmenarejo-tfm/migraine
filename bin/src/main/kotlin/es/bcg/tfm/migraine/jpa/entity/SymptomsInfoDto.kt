package es.bcg.tfm.migraine.jpa.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity(name = "symptoms_info")
data class SymptomsInfoDto(

        @Id
        @Column(name = "symptom_id", nullable = false, length = 5)
        val id: Int,

        @Column(name = "name_es", nullable = false)
        val name: String

)
