package es.bcg.tfm.migraine.security.controller.response

import io.swagger.annotations.ApiModelProperty

data class LoginResponse (

        @ApiModelProperty(name = "token", value = "Token", example = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJjbGllbnRJ" +
                "ZCI6ImJvcmphLmNvbG1lbmFyZWpvQGdydXBvc2FudGFuZGVyLmNvbSIsImlhdCI6MTU0NTIzNzMzNiwibmJmIjoxNTQ1MjM3MzM2LC" +
                "JleHAiOjE1NDUyNDA5MzZ9.4yeB-fyb8MON01DsVfcnwBjz3jncsfBt1_5y9CQIdK27uS02n_xf8kZqrBJxMhVc9F6z_cRVe7iwLcp" +
                "WX2Yk_w")
        val token : String

)