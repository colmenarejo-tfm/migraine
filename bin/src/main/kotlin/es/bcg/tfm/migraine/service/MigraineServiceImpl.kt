package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.controller.model.v1.migraine.response.SessionResponse
import es.bcg.tfm.migraine.controller.model.v1.migraine.response.SymptomResponse
import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.entity.SymptomsDto
import es.bcg.tfm.migraine.jpa.projection.MigraineProjection
import es.bcg.tfm.migraine.jpa.repositoty.E4SessionsRepository
import es.bcg.tfm.migraine.jpa.repositoty.MigraineRepository
import es.bcg.tfm.migraine.jpa.repositoty.SymptomsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.stream.Collectors

@Service
class MigraineServiceImpl : MigraineService {

    private val migraineRepository : MigraineRepository

    private val symptomsRepository : SymptomsRepository

    private val sessionRepository : E4SessionsRepository

    @Autowired
    constructor(migraineRepository : MigraineRepository, symptomsRepository : SymptomsRepository, e4sessionRepository : E4SessionsRepository){
        this.migraineRepository = migraineRepository
        this.symptomsRepository = symptomsRepository
        this.sessionRepository = e4sessionRepository
    }

    override fun migraines(patientId : String): List<MigraineProjection> {
        // TODO filter duplicated (when patient id, migraine id and session id equals)
        return migraineRepository.findAllMigrainesByPatient(patientId)
    }

    override fun migrainesDto(patientId: String): List<MigraineResponse> {
        val migraines = migraineRepository.findAllByPatientId(patientId)

        val migrainesResponses = mutableListOf<MigraineResponse>()

        for (migraine in migraines){
            var symptoms = symptomsRepository.findAllByPatientIdAndMigraineId(patientId, migraine.migraineId)
                    .stream().map { symptom
                        -> SymptomResponse(symptom.symptomId, symptom.name(), symptom.symptomStartTime, symptom.symptomEndTime) }
                    .collect(Collectors.toList())

            var sessions : List<SessionResponse>? = null
            if (migraine.painStartTime != null && migraine.painEndTime != null) {
                sessions = sessionRepository
                        .findAllByPatientIdAndStartTimeIsBeforeAndEndTimeIsAfter(patientId,
                                migraine.painStartTime!!, migraine.painEndTime!!).stream().map { session ->
                            SessionResponse(session.sessionId, session.startTime, session.endTime, session.tempQuality,
                            session.edaQuality, session.hrQuality)
                        }
                        .collect(Collectors.toList())
            }

            val migraine = MigraineResponse(migraine.patientId, migraine.migraineId, falseEpisodeTime = migraine.falseEpisodeTime,
            painStartTime = migraine.painStartTime, painEndTime = migraine.painEndTime, symtoms = symptoms, sessions = sessions)

            migraine.calculateType()

            migrainesResponses.add(migraine)
        }
        return migrainesResponses
    }

}