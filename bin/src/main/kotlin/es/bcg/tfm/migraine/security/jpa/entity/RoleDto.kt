package es.bcg.tfm.migraine.security.jpa.entity

import org.hibernate.annotations.Type
import javax.persistence.Column
import javax.persistence.Id


// TODO uncomment to create users management
// @Entity(name = "roles")
// @TypeDef(name = "json", typeClass = JsonBinaryType::class)
data class RoleDto (

        @Id
        @Column(nullable = true, length = 50)
        var name : String,

        @Type(type = "json")
        @Column(nullable =  false, columnDefinition = "json")
        var permits : PermitsDtoJson

)

data class PermitsDtoJson (var all : Boolean)