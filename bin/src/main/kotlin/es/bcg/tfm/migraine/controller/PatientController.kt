package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.controller.model.v1.request.PatientPersonalInformationRequest
import es.bcg.tfm.migraine.jpa.projection.PatientProjection
import es.bcg.tfm.migraine.service.PatientService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.RequestMethod.POST
import javax.validation.Valid
import javax.validation.constraints.NotNull

@Api(tags = ["Patients"])
@RestController
class PatientController {

    @Autowired
    private lateinit var service : PatientService

    @ApiOperation(value = "Get all patients")
    @RequestMapping(value = ["/patients"], method = [GET], produces = ["application/json"])
    fun getPatients(@RequestParam(required = true) studyId : Int) : MinimalPatientsResponse {
        return service.getPatients(studyId)
    }

    @RequestMapping(value = ["/patients/{id}"], method = [GET])
    fun getPatient(@PathVariable(name = "id", required = true, value = "id") id : String) : PatientResponse {
        return service.getPatient(id)
    }

//    @RequestMapping(value = ["/patients/{id}"], method = [POST])
//    fun savePersonalInfo(@PathVariable(name = "id", required = true, value = "id") id : String,
//                         @NotNull @RequestBody(
//                                 required = true) @Valid request : PatientPersonalInformationRequest) : PatientResponse? {
//        return service.postPatient(id, request)
//    }

}
