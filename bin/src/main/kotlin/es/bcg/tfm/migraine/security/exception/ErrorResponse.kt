package es.bcg.tfm.migraine.security.exception

/**
 * POC this class is useless
 */
data class ErrorResponse (

        val error : String,

        val description : String?

)