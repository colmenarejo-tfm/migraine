package es.bcg.tfm.migraine.controller.model.v1.request

data class PatientPersonalInformationRequest(

        val name: String,

        val surname: String,

        val secondSurname: String?,

        val phone: String,

        val comment: String?

)