// TODO uncomment
//package es.bcg.tfm.migraine.config
//
//import es.bcg.tfm.migraine.security.config.TokenAuthenticationFilter
//import es.bcg.tfm.migraine.security.service.TokenAuthenticationUserDetailsService
//import es.bcg.tfm.migraine.security.service.UserDetailsServiceImpl
//import mu.KotlinLogging
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.context.annotation.Bean
//import org.springframework.context.annotation.Configuration
//import org.springframework.core.annotation.Order
//import org.springframework.http.HttpMethod
//import org.springframework.security.authentication.AuthenticationManager
//import org.springframework.security.authentication.AuthenticationProvider
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
//import org.springframework.security.config.annotation.web.builders.HttpSecurity
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
//import org.springframework.security.config.http.SessionCreationPolicy
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
//import org.springframework.security.crypto.password.PasswordEncoder
//import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider
//import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter
//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//class SecurityConfig {
//
//    private val log = KotlinLogging.logger {}
//
//    private lateinit var service: UserDetailsServiceImpl
//
//    private lateinit var passwordEncoder: PasswordEncoder
//
//    @Autowired
//    fun constructor(service: UserDetailsServiceImpl, passwordEncoder: PasswordEncoder) {
//        this.service = service
//        this.passwordEncoder = passwordEncoder
//    }
//
//    @Bean
//    fun passwordEncoder(): PasswordEncoder {
//        return BCryptPasswordEncoder(10)
//    }
//
//    @Configuration
//    @Order(1)
//    inner class BasicAuthConfig : WebSecurityConfigurerAdapter() {
//
//        @Throws(Exception::class)
//        override fun configure(http: HttpSecurity) {
//            http.csrf().disable()
//                    .antMatcher("/login")
//                    .authorizeRequests()
//                    .anyRequest()
//                    .authenticated()
//                    .and()
//                    .httpBasic()
//                    .and()
//                    .sessionManagement()
//                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//        }
//
//        @Throws(Exception::class)
//        override fun configure(auth: AuthenticationManagerBuilder) {
//            auth.userDetailsService(service).passwordEncoder(passwordEncoder)
//        }
//    }
//
//
//    @Configuration
//    @Order(2)
//    inner class TokenAuthConfig @Autowired
//    constructor(private val service: TokenAuthenticationUserDetailsService) : WebSecurityConfigurerAdapter() {
//
//        @Throws(Exception::class)
//        override fun configure(http: HttpSecurity) {
//            http
//                    .antMatcher("/bookings")
//                    .authorizeRequests()
//                    .mvcMatchers(HttpMethod.POST, "/bookings")
//                    .anonymous()
//                    .anyRequest()
//                    .authenticated()
//                    .and()
//                    .addFilterBefore(authFilter(), RequestHeaderAuthenticationFilter::class.java)
//                    .authenticationProvider(preAuthProvider())
//                    .sessionManagement()
//                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                    .and()
//                    .csrf().disable()
//        }
//
//
//        @Bean
//        fun authFilter(): TokenAuthenticationFilter {
//            return TokenAuthenticationFilter()
//        }
//
//        @Bean
//        @Throws(Exception::class)
//        override fun authenticationManagerBean(): AuthenticationManager {
//            return super.authenticationManagerBean()
//        }
//
//        @Bean
//        fun preAuthProvider(): AuthenticationProvider {
//            val provider = PreAuthenticatedAuthenticationProvider()
//            provider.setPreAuthenticatedUserDetailsService(service)
//            return provider
//        }
//    }
//}