package es.bcg.tfm.migraine.jpa.entity

import java.util.*
import javax.persistence.*

@Entity(name = "patients")
data class PatientDto(

        @Id
        @Column(name = "patient_id", nullable = false, length = 9)
        var id: String,

        @Column(nullable = false, length = 50)
        var user: String,

        @Column(name = "pwd", nullable = false, length = 40)
        var password: String,

        @Column(nullable = false)
        var startDate: Date,

        var endDate: Date? = null,

        @Column(nullable = false, length = 5)
        var studyId: Int,

        @Column(length = 255)
        var name: String? = null,

        @Column(length = 255)
        var surname1: String? = null,

        @Column(length = 255)
        var surname2: String? = null,

        @Column(length = 255)
        var phone: String? = null,

        @Column(length = 6)
        var points: Int = 0,

        @Column(length = 2047)
        var comments: String? = null

)