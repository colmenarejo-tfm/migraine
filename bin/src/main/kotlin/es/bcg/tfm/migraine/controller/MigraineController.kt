package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.projection.MigraineProjection
import es.bcg.tfm.migraine.service.MigraineService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@Api(tags = ["Migraine"])
@RestController
class MigraineController {

    @Autowired
    private lateinit var service : MigraineService

//    @ApiOperation(value = "Get all migraines")
//    @RequestMapping(value = ["/migraines"], method = [RequestMethod.GET], produces = ["application/json"])
//    fun getPatients(@AuthenticationPrincipal principal : TokenUserDetails,
//                    @RequestParam(required = true) patientId : String) : List<MigraineProjection> {
//        return service.migraines(patientId)
//    }

    @ApiOperation(value = "Get all migraines")
    @RequestMapping(value = ["/migraines"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getMigraines(@RequestParam(required = true) patientId : String) : List<MigraineProjection> {
        return service.migraines(patientId)
    }

    @ApiOperation(value = "Get all migraines")
    @RequestMapping(value = ["/migraines-complete"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getMigrainesDto(@RequestParam(required = true) patientId : String) : List<MigraineResponse> {
        return service.migrainesDto(patientId)
    }

}