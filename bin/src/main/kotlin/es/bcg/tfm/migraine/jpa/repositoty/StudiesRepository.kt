package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.StudiesDto
import org.springframework.data.jpa.repository.JpaRepository

interface StudiesRepository : JpaRepository<StudiesDto, String>