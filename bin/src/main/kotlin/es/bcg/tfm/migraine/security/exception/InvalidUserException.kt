package es.bcg.tfm.migraine.security.exception

class InvalidUserException : RuntimeException {

    constructor(message: String, ex: Exception?): super(message, ex) {}

    constructor(message: String): super(message) {}

    constructor(ex: Exception): super(ex) {}
}