package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.request.PatientPersonalInformationRequest
import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.jpa.projection.PatientProjection

interface PatientService {

    fun getPatients(studyId: Int): MinimalPatientsResponse

    fun getPatient(id: String): PatientResponse

    fun postPatient(id: String, personalInfo: PatientPersonalInformationRequest): PatientResponse?

}