package es.bcg.tfm.migraine.controller.model.v1.study.response

data class StudyResponse(

        var id: Int,

        var name: String,

        var startDate: String? = null

)