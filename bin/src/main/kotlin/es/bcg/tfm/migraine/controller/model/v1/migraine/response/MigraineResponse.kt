package es.bcg.tfm.migraine.controller.model.v1.migraine.response

import java.util.*

data class MigraineResponse (

        val patientId: String,

        val migraineId: Int,

        var type : String = "",

        var monitorized : Boolean = false,

        var falseEpisodeTime: Date? = null,

        var painStartTime: Date? = null,

        var painEndTime: Date? = null,

        var symtoms : List<SymptomResponse>?,

        var sessions : List<SessionResponse>?

) {
    fun calculateType() {
        if (falseEpisodeTime != null){
            type = "FALSA"
//        } else if (){

        }
    }
}