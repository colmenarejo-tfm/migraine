package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.jpa.entity.StudiesDto

interface StudiesService {

    fun studies() : StudiesResponse

    fun study(id : String) : StudiesDto

}