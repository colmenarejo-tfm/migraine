package es.bcg.tfm.migraine.security.jpa.repository

import es.bcg.tfm.migraine.security.jpa.entity.RoleDto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

// TODO remove to create users management
@NoRepositoryBean
interface RoleRepository : JpaRepository<RoleDto, String>