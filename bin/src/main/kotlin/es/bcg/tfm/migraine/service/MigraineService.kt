package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.migraine.response.MigraineResponse
import es.bcg.tfm.migraine.jpa.entity.MigraineDto
import es.bcg.tfm.migraine.jpa.projection.MigraineProjection

interface MigraineService {

    fun migraines(patientId : String) : List<MigraineProjection>

    fun migrainesDto(patientId : String) : List<MigraineResponse>

}