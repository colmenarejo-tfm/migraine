package es.bcg.tfm.migraine.controller.model.v1.resquest

import io.swagger.annotations.ApiModelProperty


data class LoginRequest (

        @ApiModelProperty(name = "clientId", value = "Client id", example = "borja.colmenarejo@gruposantander.com",
                required = true)
        val clientId : String,

        @ApiModelProperty(name = "password", value = "Client password", example = "****", required = true)
        val password : String
)