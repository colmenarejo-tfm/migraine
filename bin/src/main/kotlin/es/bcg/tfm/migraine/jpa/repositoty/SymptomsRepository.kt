package es.bcg.tfm.migraine.jpa.repositoty

import es.bcg.tfm.migraine.jpa.entity.SymptomsDto
import es.bcg.tfm.migraine.jpa.entity.SymptonsId
import org.springframework.data.jpa.repository.JpaRepository

interface SymptomsRepository : JpaRepository<SymptomsDto, SymptonsId> {


    fun findAllByPatientIdAndMigraineId(patientId: String, migraineId: Int) : List<SymptomsDto>

}