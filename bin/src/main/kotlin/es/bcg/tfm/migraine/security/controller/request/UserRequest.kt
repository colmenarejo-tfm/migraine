package es.bcg.tfm.migraine.deleted.controller.request

import io.swagger.annotations.ApiModelProperty

data class UserRequest (

        @ApiModelProperty(name = "user", value = "User id", example = "borja.colmenarejo@gruposantander.com",
                required = true)
        val user : String,

        @ApiModelProperty(name = "password", value = "User password", example = "****", required = true)
        val password : String,

        @ApiModelProperty(name = "role", value = "User role", example = "default", required = false)
        val role : String = "default"

)