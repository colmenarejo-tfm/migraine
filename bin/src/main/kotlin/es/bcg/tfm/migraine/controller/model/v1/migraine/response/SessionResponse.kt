package es.bcg.tfm.migraine.controller.model.v1.migraine.response

import java.util.*

data class SessionResponse (

        val sessionId: String,

        val startTime: Date,

        val endTime: Date,

        val tempQuality: String,

        val edaQuality: String,

        val hrQuality: String

)