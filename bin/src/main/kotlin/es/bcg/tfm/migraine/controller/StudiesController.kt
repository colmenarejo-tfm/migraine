package es.bcg.tfm.migraine.controller

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.jpa.entity.StudiesDto
import es.bcg.tfm.migraine.service.StudiesService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Studies"])
@RestController
class StudiesController {

    @Autowired
    private lateinit var service : StudiesService


    @ApiOperation(value = "Get all studies")
    @RequestMapping(value = ["/studies"], method = [RequestMethod.GET], produces = ["application/json"])
    fun getPatients() : StudiesResponse {
        return service.studies()
    }


    @RequestMapping(value = ["/studies/{id}"], method = [RequestMethod.GET])
    fun getPatient(@PathVariable(name = "id", required = true, value = "id") id : String) : StudiesDto {
        return service.study(id)
    }

}