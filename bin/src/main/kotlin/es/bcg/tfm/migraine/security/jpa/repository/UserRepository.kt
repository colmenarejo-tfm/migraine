package es.bcg.tfm.migraine.security.jpa.repository

import es.bcg.tfm.migraine.security.jpa.entity.UserDto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

// TODO remove to create users management
@NoRepositoryBean
interface UserRepository : JpaRepository<UserDto, String> {

    fun findByIdAndPassword(id: String, password: String) : UserDto?

    fun findByUsername(username: String) : UserDto?

}