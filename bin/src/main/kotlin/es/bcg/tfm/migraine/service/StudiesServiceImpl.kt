package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.study.response.StudiesResponse
import es.bcg.tfm.migraine.controller.model.v1.study.response.StudyResponse
import es.bcg.tfm.migraine.jpa.entity.StudiesDto
import es.bcg.tfm.migraine.jpa.repositoty.StudiesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.text.SimpleDateFormat



@Service
class StudiesServiceImpl : StudiesService {

    private val studiesRepository: StudiesRepository

    var format = SimpleDateFormat("yyyy-MM-dd")

    private val mapper = { study: StudiesDto -> StudyResponse(study.id, study.name, format.format(study.startDate)) }

    @Autowired
    constructor(studiesRepository: StudiesRepository) {
        this.studiesRepository = studiesRepository
    }

    override fun studies(): StudiesResponse {

        val studies = studiesRepository.findAll()

        val studiesResponse : MutableList<StudyResponse> =  mutableListOf()
        for (study : StudiesDto in  studies){
            studiesResponse.add(mapper(study))
        }

        return StudiesResponse(studiesResponse)
    }

    override fun study(id: String): StudiesDto {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        // return studiesRepository.findById(id).get()
    }

}