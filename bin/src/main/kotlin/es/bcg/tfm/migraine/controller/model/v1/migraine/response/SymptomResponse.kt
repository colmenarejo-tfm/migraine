package es.bcg.tfm.migraine.controller.model.v1.migraine.response

import java.util.*

data class SymptomResponse (

        val id: Int,

        val name: String,

        val symptomStartTime: Date,

        val symptomEndTime: Date? = null
)