package es.bcg.tfm.migraine.jpa.entity

import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity(name = "e4_sessions")
@IdClass(E4SessionsId::class)
data class E4SessionsDto(

        @Id
        @Column(nullable = false, length = 9)
        val patientId: String,

        @Id
        @Column(nullable = false, length = 8)
        val sessionId: String,

        @Column(nullable = true, length = 6)
        val deviceId: String?,

        @Column(nullable = false)
        @Temporal(TemporalType.TIMESTAMP)
        val startTime: Date,

        @Column(nullable = false)
        @Temporal(TemporalType.TIMESTAMP)
        val endTime: Date,

        @Column(nullable = true, length = 6)
        val device: String?,

        @Column(nullable = true, length = 4)
        val label: Int?,

        @Column(nullable = true, length = 1)
        val exitCode: Int?,

        @Column(nullable = true, length = 1)
        val status: Int?,

        @Column(nullable = false, columnDefinition = "enum('bad','medium','good')")
        val tempQuality: String,

        @Column(nullable = false, columnDefinition = "enum('bad','medium','good')")
        val edaQuality: String,

        @Column(nullable = false, columnDefinition = "enum('bad','medium','good')")
        val hrQuality: String

)

data class E4SessionsId(
        var patientId : String = "",

        var sessionId : String = ""
) : Serializable