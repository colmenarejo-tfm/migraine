package es.bcg.tfm.migraine.security.service

// TODO uncomment
//package es.bcg.tfm.migraine.service
//
//import es.bcg.tfm.migraine.security.jpa.repository.UserRepository
//import io.jsonwebtoken.Header
//import io.jsonwebtoken.Jwts
//import org.springframework.stereotype.Service
//import java.util.*
//import io.jsonwebtoken.SignatureAlgorithm
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.beans.factory.annotation.Value
//import kotlin.collections.HashMap
//
//// TODO uncomment annotation
//// @Service
//class TokenServiceImpl : TokenService {
//
//    private val secret : String = "BORJA ES EL AMO"
//
//    private var claims: MutableMap<String, String> = HashMap()
//
//    private val userRepository : UserRepository
//
//    @Value("\${token.expiration-time}")
//    private val expirationTime : Int? = null
//
//    @Autowired
//    constructor(userRepository : UserRepository){
//        this.userRepository = userRepository
//    }
//
//    override fun getToken(user: String, role: String): String {
//        val now = Date()
//        val exp = Date(System.currentTimeMillis() + expirationTime!!)
//
//        claims.put("user", user)
//        claims.put("role", role)
//
//        return Jwts.builder().setHeaderParam(Header.TYPE, Header.JWT_TYPE).addClaims(claims as Map<String, Any>?)
//                .setIssuedAt(now).setNotBefore(now).setExpiration(exp)
//                .signWith(SignatureAlgorithm.HS512, secret).compact()
//    }
//
//}