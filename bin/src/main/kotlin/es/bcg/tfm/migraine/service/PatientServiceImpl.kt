package es.bcg.tfm.migraine.service

import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.MinimalPatientsResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientResponse
import es.bcg.tfm.migraine.controller.model.v1.patient.response.PatientSociodemoResponse
import es.bcg.tfm.migraine.controller.model.v1.request.PatientPersonalInformationRequest
import es.bcg.tfm.migraine.jpa.projection.PatientProjection
import es.bcg.tfm.migraine.jpa.repositoty.PatientRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.stream.Collectors

@Service
class PatientServiceImpl @Autowired constructor(private val patientRepository: PatientRepository) : PatientService {

    private val log = KotlinLogging.logger {}

    var format = SimpleDateFormat("dd/MM/yyyy")

    private val mapper = { patient: PatientProjection ->
        PatientSociodemoResponse(patient.user, patient.fullName(), patient.phone, patient.sex,
                format.format(patient.birthdayDate), format.format(patient.startDate), format.format(patient.endDate), format.format(patient.studyStartDate),
                patient.cognitiveReserve, patient.studyLevel, patient.employment, patient.diagnosis, patient.crisisFrequency, patient.background, patient.startAge,
                patient.hypertension, patient.diabetes, patient.dyslipidemia, patient.smoker, patient.alcohol, patient.painQuality, patient.menstruation,
                patient.coffe, patient.stress, patient.anxiety, patient.depression, patient.sleepDisturbance, patient.imc, patient.hit
        )
    }

    override fun getPatients(studyId: Int): MinimalPatientsResponse {
        var patientsProjection : List<PatientProjection> =  mutableListOf()
        try {
            patientsProjection = patientRepository.findAllPatientsByStudy(studyId) as List
        } catch (e : Exception){
            val ids = patientRepository.findAllPatientsIdByStudy(studyId)
            if (!ids.isEmpty()) {
                patientsProjection = mutableListOf()
                for (id in ids) {
                    try {
                        val patient = patientRepository.findPatientById(id)
                        if (patient.isPresent){
                            patientsProjection.add(patient.get())
                        }
                    } catch (e : Exception) {
                        log.debug("Error in patient whit id ${id}: ${e}")
                    }

                }
            }
        }
        return MinimalPatientsResponse(patientsProjection.stream().map { patient ->
            MinimalPatientResponse(patient.id,
                    patient.user, patient.fullName(),
                    patient.phone, patient.sex)
        }.collect(Collectors.toList()))
    }

    override fun getPatient(id: String): PatientResponse {
        val optional = patientRepository.findPatientById(id)

        if (optional.isPresent) {
            return PatientResponse(mapper(optional.get()))
        }

        return PatientResponse(null)
    }

    override fun postPatient(id: String, personalInfo: PatientPersonalInformationRequest): PatientResponse? {
        val patientOptional = patientRepository.findById(id)

//        if (patientOptional.isPresent){
//            var patient = patientOptional.get()
//            patient.name = personalInfo.name
//            patient.surname1 = personalInfo.surname
//            patient.surname2 = personalInfo.secondSurname
//            patient.phone = personalInfo.phone
//            patient.comments = personalInfo.comment
//
//            patientRepository.save(patient)
//            return mapper(patient)
//        }
        return null
    }

}