package es.bcg.tfm.migraine.controller.model.v1.patient.response

data class PatientResponse (
        val sociodemo : PatientSociodemoResponse?
)