package es.bcg.tfm.migraine.security.controller

//package es.bcg.tfm.migraine.controller
//
//import es.bcg.tfm.migraine.security.controller.response.LoginResponse
//import es.bcg.tfm.migraine.security.model.TokenUserDetails
//import io.swagger.annotations.Api
//import io.swagger.annotations.ApiOperation
//import io.swagger.annotations.ApiParam
//import org.springframework.data.repository.NoRepositoryBean
//import org.springframework.security.core.annotation.AuthenticationPrincipal
//import org.springframework.web.bind.annotation.RequestHeader
//import org.springframework.web.bind.annotation.RequestMapping
//import org.springframework.web.bind.annotation.RequestMethod.POST
//import org.springframework.web.bind.annotation.RestController
//
//@Api(tags = ["Login"])
//// TODO uncomment annotation
//// @RestController
//class LoginController {
//
//    @ApiOperation(value = "Generate new token")
//    @RequestMapping(value = ["/login"], method = [POST], produces = ["application/json"])
//    fun login(@AuthenticationPrincipal principal : TokenUserDetails,
//              @ApiParam(value = "authorization",
//                      example = "Basic YWRtaW46YWRtaW4=",
//                      required = true)
//              @RequestHeader(value = "authorization", required = true) authorization : String): LoginResponse {
//        return LoginResponse(principal.token)
//    }
//
//}