package es.bcg.tfm.migraine.security.jpa.entity

import javax.persistence.*

// TODO uncomment to create users management
// @Entity(name = "users")
data class UserDto(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id : Long = 0,

        @Column(nullable = false, length = 7)
        var username : String,

        @Column(nullable = false, length = 50)
        var password: String,

        @Column(nullable = false, length = 50)
        var role : String

)